# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower) -
# * move(from_tower, to_tower) -
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  # You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
  # pile to select a disc from. The pile should be the index of a tower in your
  # `@towers` array. Use gets
  # (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
  # answer. Similarly, find out which pile the user wants to move the disc to.
  # Next, you'll want to do different things depending on whether or not the move
  # is valid. Finally, if they have succeeded in moving all of the discs to
  # another pile, they win! The loop should end.
  def play
    puts "Welcome to Towers Of Hanoi."
    puts "Make a move (towers 0, 1, and 2)."
    until won?
      render
      valid = false
      until valid
        # get input from user
        from_tower = get_input("From")
        to_tower = get_input("To")
        # valid_move?, if not valid get input again
        if !valid_move?(from_tower, to_tower)
          puts "Move invalid! Try again!"
        else
          move(from_tower, to_tower)
          valid = true
        end
      end
    end
    render
    puts "You won!"
  end

  def move(from_tower, to_tower)
    towers[to_tower].push(towers[from_tower].pop)
  end

  def valid_move?(from_tower, to_tower)
    if towers[from_tower].empty?
      false
    elsif towers[to_tower].empty?
      true
    elsif towers[to_tower].last < towers[from_tower].last
      false
    else
      true
    end
  end

  def won?
    if towers.last == [3,2,1] || towers[1] == [3,2,1]
      true
    else
      false
    end
  end

  private

  def render
    i = towers.length-1
    while i >= 0
      j = 0
      while j < towers.length
        if !(towers[j][i] == nil)
          print towers[j][i]
        else
          print "_"
        end
        print "\t"
        j+=1
      end
      print "\n"
      i-=1
    end
  end

  def get_input(str)
    print "#{str} Tower: "
    gets.chomp.to_i
  end

end
